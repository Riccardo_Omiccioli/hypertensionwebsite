<?php

    // header("location: maintenance.php");

    if(!isset($_SESSION)) {
        session_start();
    }

    if (!isset($_SESSION["user"])){
        header("location: login.php");
        exit;
    }

    require_once($_SERVER['DOCUMENT_ROOT']."/../private/path.php");
    $params["page"] = PAGE."data.php";
    $params["title"] = "Home";
    $params["css"] = ["mainStyleSheet.css", "dataStyleSheet.css"];
    // To get file modified time
    // ."?version=".(date("Y-m-d_h:i:s", filemtime("/var/www/hypertension/public".JAVASCRIPT."mainPageScript.js")))
	$params["scriptjs"] = ["mainPageScript.js", "dataPageScript.js"];
    require_once(TEMPLATE."base.php");

    //header("location: data.php");
    //exit;

?>
