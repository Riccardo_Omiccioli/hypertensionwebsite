function toggleDrawer() {
    if($("body > main > aside").hasClass("open")) {
        $("body > main > aside").removeClass("open").animate({left: "-=70%"}, 300, "linear");
    } else {
        $('body > main > aside').addClass("open").animate({left: "+=70%"}, 300, "linear");
    }
}

$(document).ready(function(){

    if(document.getElementById('menu')) {
        document.getElementById('menu').addEventListener('click', function(e){
            e.preventDefault();
            toggleDrawer();
        });

        document.getElementById('logout').addEventListener('click', function (e) {
            e.preventDefault();
            var oReq = new XMLHttpRequest();
            oReq.open("POST", "/api/logout.php");
            oReq.send();
            oReq.onreadystatechange = function() {
                if(this.readyState != 4) {
                    return;
                }
                console.log(this.status);
                console.log(this.response);
                if (this.status == 200) {
                    window.location.href = "/";
                } else if(this.status == 400){
                    errorMessage.textContent = "Error 400: something went wrong";
                } else if(this.status == 500){
                    errorMessage.textContent = "Error 500: something went wrong";
                }
            }
        });
    }

});