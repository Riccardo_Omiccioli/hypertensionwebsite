$(document).ready(function(){

    if(document.getElementById('linkCodeIn')) {
        document.getElementById('linkCodeIn').addEventListener('input', function (e) {
            var foo = $(this).val().split("-").join(""); // remove hyphens
            if (foo.length > 0) {
            foo = foo.match(new RegExp('.{1,3}', 'g')).join("-");
            }
            $(this).val(foo);
        });
    }

    $(document.getElementById("showPassword")).click(function(event){
        event.preventDefault();
        if($(document.getElementById("passwordIn")).attr("type") == "password") {
            $(document.getElementById("passwordIn")).attr("type", "text");
            $(document.getElementById("newPasswordIn")).attr("type", "text");
            $(document.getElementById("newRePasswordIn")).attr("type", "text");
        } else {
            $(document.getElementById("passwordIn")).attr("type", "password");
            $(document.getElementById("newPasswordIn")).attr("type", "password");
            $(document.getElementById("newRePasswordIn")).attr("type", "password");
        }
    });

    $(document.getElementById("save")).click(function(event){
        event.preventDefault();
        var errorMessage = document.getElementById("errorMessage");
        var successMessage = document.getElementById("successMessage");

        var regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$/;
        var regexPassword = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;

        var email = document.getElementById("emailIn").value;
        var password = document.getElementById("passwordIn").value;
        var newPassword = document.getElementById("newPasswordIn").value;
        var newRePassword = document.getElementById("newRePasswordIn").value;
        if(document.getElementById('linkCodeIn')) {
            var linkCode = document.getElementById("linkCodeIn").value;
        }
        if (!email.match(regexEmail)) {
            successMessage.classList.remove("successMessageShow");
            errorMessage.textContent = "Inserire un indirizzo email valido";
            errorMessage.classList.add("errorMessageShow");
	    } else if (!password.match(regexPassword)) {
            successMessage.classList.remove("successMessageShow");
            errorMessage.textContent = "Inserire la propria password attuale";
            errorMessage.classList.add("errorMessageShow");
        } else if (newPassword !== newRePassword) {
            successMessage.classList.remove("successMessageShow");
            errorMessage.textContent = "La conferma della nuova password non corrisponde";
            errorMessage.classList.add("errorMessageShow");
        } else if (email.match(regexEmail) && password.match(regexPassword)) {
            var sendParams = new URLSearchParams();
            sendParams.append('email', email);
            sendParams.append('password', password);
            if(document.getElementById('linkCodeIn')) {
                if (linkCode !== "") {
                    sendParams.append('linkCode', linkCode);
                }
            }
            if (newPassword.match(regexPassword) && newPassword === newRePassword) {
                sendParams.append('newPassword', newPassword);
            }

            var oReq = new XMLHttpRequest();
            oReq.open("POST", "/api/saveUserSettings.php");
            oReq.send(sendParams);

            oReq.onreadystatechange = function() {
                if (this.readyState != 4) {
                    return;
                }
                console.log(this.status);
                console.log(this.response);
                if (this.status == 200) {
                    errorMessage.textContent = "";
                    errorMessage.classList.remove("errorMessageShow");
                    successMessage.textContent = "Impostazioni salvate";
                    successMessage.classList.add("successMessageShow");
                    document.getElementById("passwordIn").value = "";
                    document.getElementById("newPasswordIn").value = "";
                    document.getElementById("newRePasswordIn").value = "";
                } else if(this.status == 204) {
                    window.location.href = "/";
                } else if(this.status == 400){
                    var error = "richiesta non valida";
                    try {
                        var response = JSON.parse(this.response);
                        error = response.error;
                    } catch (e) {
                    }
                    errorMessage.textContent = "Errore: " + error;
                } else if(this.status == 401){
                    errorMessage.textContent = "Errore: email o password errati";
                } else if(this.status == 500){

                } else {

                }
            }
        }
    });

});