function refreshTable() {
    var idUser = document.getElementById('idUser').value;
    var oReq = new XMLHttpRequest();
    var sendParams = new URLSearchParams();
    sendParams.append("idUser", idUser);
    oReq.open("POST", "/api/getUser.php");
    oReq.send(sendParams);
    oReq.onreadystatechange = function() {
        if(this.readyState != 4) {
            return;
        }
        console.log("status: " + this.status);
        console.log("response: " + this.response);
        if (this.status == 200) {
            user = JSON.parse(this.response);
            document.getElementById('idTelegramData').innerHTML = user["idTelegram"];
            document.getElementById('emailData').innerHTML = user["email"];
            document.getElementById('doctorData').innerHTML = user["idDoctor"] === 0 || user["idDoctor"] === null ? "No" : "Sì";
            document.getElementById('adminData').innerHTML = user["idAdmin"] === 0 || user["idAdmin"] === null ? "No" : "Sì";
        } else if(this.status == 400){
            writeError("Utente non trovato");
        } else if(this.status == 500){
            writeError("Errore 500");
        }
    }
}

function clearTable() {
    document.getElementById('idTelegramData').innerHTML = "";
    document.getElementById('emailData').innerHTML = "";
    document.getElementById('doctorData').innerHTML = "";
    document.getElementById('adminData').innerHTML = "";
}

function writeError(message) {
    var errorMessage = document.getElementById("errorMessage");
    var successMessage = document.getElementById("successMessage");
    successMessage.textContent = "";
    successMessage.classList.remove("successMessageShow");
    errorMessage.textContent = message;
    errorMessage.classList.add("errorMessageShow");
}

function writeSuccess(message) {
    var errorMessage = document.getElementById("errorMessage");
    var successMessage = document.getElementById("successMessage");
    errorMessage.textContent = "";
    errorMessage.classList.remove("errorMessageShow");
    successMessage.textContent = message;
    successMessage.classList.add("successMessageShow");
}

$(document).ready(function(){

    document.getElementById('idUser').addEventListener('input', function (e) {
        var input = $(this).val().split("-").join(""); // remove hyphens
        var idUser = input;
        if (input.length > 0) {
            input = input.match(new RegExp('.{1,3}', 'g')).join("-");
        }
        // Refresh input
        $(this).val(input);
        console.log(input.length);
        if(idUser.length === 12) {
            writeSuccess("");
            refreshTable();
        } else {
            clearTable();
        }
    });

    document.getElementById('remove').addEventListener('click', function (e) {
        e.preventDefault();
        var idUserInput = document.getElementById("idUser");
        var idUser = document.getElementById("idUser").value;
        if (idUser !== null) {
            if (confirm("Sei sicuro di voler rimuovere l'utente: " + idUser + " ?")) {
                var oReq = new XMLHttpRequest();
                var sendParams = new URLSearchParams();
                sendParams.append("idUser", idUser);
                oReq.open("POST", "/api/removeUser.php");
                oReq.send(sendParams);
                oReq.onreadystatechange = function() {
                    if(this.readyState != 4) {
                        return;
                    }
                    // console.log(this.status);
                    // console.log(this.response);
                    if (this.status == 200) {
                        writeSuccess("Utente " + idUser + " rimosso con successo");
                        idUserInput.value = "";
                        clearTable();
                    }else if(this.status == 400){
                        writeError("Error 400: something went wrong");
                    } else if(this.status == 500){
                        writeError("Error 500: something went wrong");
                    }
                }
            }
        }
    });

    document.getElementById('setDoctor').addEventListener('click', function (e) {
        e.preventDefault();
        idUser = document.getElementById("idUser").value;
        if (idUser !== null) {
            if (confirm("Sei sicuro di voler modificare i permessi di medico all'utente: " + idUser + " ?")) {
                var oReq = new XMLHttpRequest();
                var sendParams = new URLSearchParams();
                sendParams.append("idUser", idUser);
                oReq.open("POST", "/api/setDoctor.php");
                oReq.send(sendParams);
                oReq.onreadystatechange = function() {
                    if(this.readyState != 4) {
                        return;
                    }
                    // console.log(this.status);
                    // console.log(this.response);
                    if (this.status == 200) {
                        writeSuccess("Permessi medico concessi");
                        refreshTable();
                    } else if (this.status == 201) {
                        writeSuccess("Permessi medico rimossi");
                        refreshTable();
                    }else if(this.status == 400){
                        writeError("Error 400: something went wrong");
                    } else if(this.status == 500){
                        writeError("Error 500: something went wrong");
                    }
                }
            }
        }
    });

    document.getElementById('setAdmin').addEventListener('click', function (e) {
        e.preventDefault();
        idUser = document.getElementById("idUser").value;
        if (idUser !== null) {
            if (confirm("Sei sicuro di voler modificare i permessi di admin all'utente: " + idUser + " ?")) {
                var oReq = new XMLHttpRequest();
                var sendParams = new URLSearchParams();
                sendParams.append("idUser", idUser);
                oReq.open("POST", "/api/setAdmin.php");
                oReq.send(sendParams);
                oReq.onreadystatechange = function() {
                    if(this.readyState != 4) {
                        return;
                    }
                    // console.log(this.status);
                    // console.log(this.response);
                    if (this.status == 200) {
                        writeSuccess("Permessi admin concessi");
                        refreshTable();
                    } else if (this.status == 201) {
                        writeSuccess("Permessi admin rimossi");
                        refreshTable();
                    }else if(this.status == 400){
                        writeError("Error 400: something went wrong");
                    } else if(this.status == 500){
                        writeError("Error 500: something went wrong");
                    }
                }
            }
        }
    });

});