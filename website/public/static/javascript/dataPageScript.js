$(document).ready(function(){

    let labels = [];
    let systolic = [];
    let diastolic = [];
    let thSystolic = [];
    let thDiastolic = [];

    let labelsWindow = [];
    let systolicWindow = [];
    let diastolicWindow = [];
    let thSystolicWindow = [];
    let thDiastolicWindow = [];

    let data = {
        labels: labelsWindow,
        datasets: [{
            label: 'Sistolica',
            backgroundColor: 'rgba(59, 107, 254, 1)',
            fill: false,
            borderColor: 'rgb(59, 107, 254)',
            data: systolicWindow,
            tension: 0.4
            },{
            label: 'Diastolica',
            backgroundColor: 'rgba(62, 159, 255, 1)',
            fill: false,
            borderColor: 'rgb(62, 159, 255)',
            data: diastolicWindow,
            tension: 0.4
            },{
            label: 'Valore limite pressione sistolica',
            backgroundColor: 'rgba(87, 120, 73, 0.3)',
            fill: false,
            borderColor: 'rgb(87, 120, 73)',
            borderWidth: 2,
            borderDash: [5, 5],
            pointRadius: 0,
            data: thSystolicWindow,
            stepped: false,
            tension: 0,
            hidden: false
            },{
            label: 'Valore limite pressione diastolica',
            backgroundColor: 'rgba(119, 164, 100, 0.3)',
            fill: false,
            borderColor: 'rgb(119, 164, 100)',
            borderWidth: 2,
            borderDash: [5, 5],
            pointRadius: 0,
            data: thDiastolicWindow,
            stepped: false,
            tension: 0,
            hidden: false
        }],
    };

    const config = {
        type: 'line',
        data: data,
        options: {
            scales: {
                xAxis: {
                    ticks: {
                        font: {
                            size: 20
                        },
                        //maxTicksLimit: 5
                    }
                },
                yAxis: {
                    suggestedMin: 50,
                    suggestedMax: 130,
                    ticks: {
                        font: {
                            size: 25
                        }
                    }
                }
            },
            plugins: {
                legend: {
                    labels: {
                        font: {
                            size: 30
                        }
                    }
                }
            }
        }
    };

    let dataChart = new Chart(document.getElementById('dataChart'), config);
    let windowSize = 7;
    if(window.innerWidth > 1200) {
        windowSize = 14;
    }
    let windowIndex;
    let measurements;

    let params = (new URL(document.location)).searchParams;
    let idUser = params.get("idUser");
    var oReq = new XMLHttpRequest();
    var sendParams = new URLSearchParams();
    if(idUser !== null) {
        sendParams.append('idUser', idUser);
    }
    oReq.open("GET", "/api/pressureGet.php?" + sendParams.toString());
    oReq.send();
    oReq.onreadystatechange = function() {
        if(this.readyState != 4) {
            return;
        }
        // console.log("status: " + this.status);
        // console.log("response: " + this.response);
        if (this.status == 200) {
            tableBody = document.getElementById("dataTableBody");
            measurements = JSON.parse(this.response);
            if(measurements) {
                measurements.forEach(data => {
                    var date = new Date(data["dateTime"]);
                    labels.push(date.getDate() + "/" + (date.getMonth() + 1));
                    systolic.push(data["systolic"]);
                    diastolic.push(data["diastolic"]);
                    thSystolic.push(135);
                    thDiastolic.push(85);
                    tableBody.insertAdjacentHTML('afterbegin', "<tr><td>" +
                        data["dateTime"] + "</td><td>" +
                        data["systolic"] + "</td><td>" +
                        data["diastolic"] + "</td><td>" +
                        (data["heartRate"] !== null ? data["heartRate"] : "") +"</td></tr>");
                        //(data["stdDevSystolic"] !== null ? data["stdDevSystolic"] : "") +"</td><td>" +
                        // data["stdDevSystolic"] +"</td><td>" +
                        //(data["stdDevDiastolic"] !== null ? data["stdDevDiastolic"] : "") +"</td></tr>");
                        // data["stdDevDiastolic"] + "</td></tr>");
                });

                if(windowSize > measurements.length) {
                    windowSize = measurements.length;
                }
                for(i = measurements.length - windowSize; i < labels.length; i++) {
                    labelsWindow.push(labels[i]);
                    systolicWindow.push(systolic[i]);
                    diastolicWindow.push(diastolic[i]);
                    thSystolicWindow.push(thSystolic[i]);
                    thDiastolicWindow.push(thDiastolic[i]);
                }
                // console.log(labelsWindow);
                windowIndex = labels.length - 1;
                dataChart.update();
            }
        } else if(this.status == 400){
            console.log("Error 400: something went wrong");
        } else if(this.status == 500){
            console.log("Error 500: something went wrong");
        }
    }

    let mean = [];
    params = (new URL(document.location)).searchParams;
    idUser = params.get("idUser");
    oReq = new XMLHttpRequest();
    sendParams = new URLSearchParams();
    if(idUser !== null) {
        sendParams.append('idUser', idUser);
    }
    oReq.open("GET", "/api/pressureMean.php?" + sendParams.toString());
    oReq.send();
    oReq.onreadystatechange = function() {
        if(this.readyState != 4) {
            return;
        }
        // console.log("status: " + this.status);
        // console.log("response: " + this.response);
        if (this.status == 200) {
            tableBody = document.getElementById("meanTableBody");
            mean = JSON.parse(this.response);
            if(mean) {
                mean.forEach(data => {
                    tableBody.insertAdjacentHTML('afterbegin', "<tr><td>" +
                        (data["s1w"] !== null ? data["s1w"] : "0") + "/" + (data["d1w"] !== null ? data["d1w"] : "0") + " (" +  + (data["c1w"] !== null ? data["c1w"] : "0") + ")" + "</td><td>" +
                        (data["s1m"] !== null ? data["s1m"] : "0") + "/" + (data["d1m"] !== null ? data["d1m"] : "0") + " (" +  + (data["c1m"] !== null ? data["c1m"] : "0") + ")" + "</td><td>" +
                        (data["s3m"] !== null ? data["s3m"] : "0") + "/" + (data["d3m"] !== null ? data["d3m"] : "0") + " (" +  + (data["c3m"] !== null ? data["c3m"] : "0") + ")" + "</td><td>" +
                        (data["s6m"] !== null ? data["s6m"] : "0") + "/" + (data["d6m"] !== null ? data["d6m"] : "0") + " (" +  + (data["c6m"] !== null ? data["c6m"] : "0") + ")" + "</td></tr>");
                });
            }
        } else if(this.status == 400){
            console.log("Error 400: something went wrong");
        } else if(this.status == 500){
            console.log("Error 500: something went wrong");
        }
    }

    document.getElementById('add').addEventListener('click', function(e){
        e.preventDefault();
        var div = document.getElementById('addDiv');
        if(div.style.display === 'none') {
            div.style.display = 'block';
        } else {
            div.style.display = 'none';
        }
    });

    if(document.getElementById('idUser')) {
        document.getElementById('idUser').addEventListener('input', function (e) {
            var foo = $(this).val().split("-").join(""); // remove hyphens
            if (foo.length > 0) {
              foo = foo.match(new RegExp('.{1,3}', 'g')).join("-");
            }
            $(this).val(foo);
        });

        document.getElementById('searchSubmit').addEventListener('click', function (e) {
            e.preventDefault();
            var searchString = document.getElementById('idUser').value;
            // TODO: Add check on length and regex
            if (searchString !== null) {
                var sendParams = new URLSearchParams();
                sendParams.append('idUser', searchString);
                window.location.href = "/data.php?" + sendParams.toString();
            }
        });
    }

    document.getElementById('pressureSubmit').addEventListener('click', function (e) {
        e.preventDefault();
        var systolic = document.getElementById("systolicIn").value;
        var diastolic = document.getElementById("diastolicIn").value;
        var heartRate = document.getElementById("heartRateIn").value;
        if(systolic !== "" && diastolic !== "") {
            var sendParams = new URLSearchParams();
            sendParams.append('systolic', systolic);
            sendParams.append('diastolic', diastolic);
            if(heartRate) {
                sendParams.append('heartRate', heartRate);
            }

            var oReq = new XMLHttpRequest();
            oReq.open("POST", "/api/pressureSend.php");
            oReq.send(sendParams);

            oReq.onreadystatechange = function() {
                if(this.readyState != 4) {
                    return;
                }
                // console.log(this.status);
                // console.log(this.response);
                if (this.status == 200) {
                    //window.location.href = "/";
                    // location.reload();
                    window.location.reload();
                } else if(this.status == 400){
                    errorMessage.textContent = "Error 400: something went wrong";
                } else if(this.status == 500){
                    errorMessage.textContent = "Error 500: something went wrong";
                }
            }
        }
    });

    function getPreviousData() {
        // console.log(windowIndex);
        if(windowIndex <= windowSize) return;
        windowIndex--;
        labelsWindow.unshift(labels[windowIndex-windowSize]);
        labelsWindow.pop();
        systolicWindow.unshift(systolic[windowIndex-windowSize]);
        systolicWindow.pop();
        diastolicWindow.unshift(diastolic[windowIndex-windowSize]);
        diastolicWindow.pop();
        thSystolicWindow.unshift(thSystolic[windowIndex-windowSize]);
        thSystolicWindow.pop();
        thDiastolicWindow.unshift(thDiastolic[windowIndex-windowSize]);
        thDiastolicWindow.pop();
        // console.log(labelsWindow);
        // console.log(systolicWindow);
        // console.log(diastolicWindow);
        dataChart.data.labels.pop();
        dataChart.data.labels.push(labels[windowIndex]);
        dataChart.data.datasets[0].data.pop();
        dataChart.data.datasets[0].data.push(systolic[windowIndex]);
        dataChart.data.datasets[1].data.pop();
        dataChart.data.datasets[1].data.push(diastolic[windowIndex]);
        dataChart.data.datasets[2].data.pop();
        dataChart.data.datasets[2].data.push(thSystolic[windowIndex]);
        dataChart.data.datasets[3].data.pop();
        dataChart.data.datasets[3].data.push(thDiastolic[windowIndex]);
        dataChart.update();
    }

    function getNextData() {
            // console.log(windowIndex);
            if(windowIndex  === labels.length - 1) return;
            windowIndex++;
            labelsWindow.shift();
            labelsWindow.push(labels[windowIndex]);
            systolicWindow.shift();
            systolicWindow.push(systolic[windowIndex]);
            diastolicWindow.shift();
            diastolicWindow.push(diastolic[windowIndex]);
            thSystolicWindow.shift();
            thSystolicWindow.push(thSystolic[windowIndex]);
            thDiastolicWindow.shift();
            thDiastolicWindow.push(thDiastolic[windowIndex]);
            // console.log(labelsWindow);
            // console.log(systolicWindow);
            // console.log(diastolicWindow);
            dataChart.data.labels.pop();
            dataChart.data.labels.push(labels[windowIndex]);
            dataChart.data.datasets[0].data.pop();
            dataChart.data.datasets[0].data.push(systolic[windowIndex]);
            dataChart.data.datasets[1].data.pop();
            dataChart.data.datasets[1].data.push(diastolic[windowIndex]);
            dataChart.data.datasets[2].data.pop();
            dataChart.data.datasets[2].data.push(thSystolic[windowIndex]);
            dataChart.data.datasets[3].data.pop();
            dataChart.data.datasets[3].data.push(thDiastolic[windowIndex]);
            dataChart.update();
    }

    document.getElementById('previousData').addEventListener('click', function (e) {
        getPreviousData();
    });

    document.getElementById('nextData').addEventListener('click', function (e) {
        getNextData();
    });

    document.getElementById('fastPreviousData').addEventListener('click', function (e) {
        for (var i = 0; i < 5; i++) {
            getPreviousData();
        }
    });

    document.getElementById('fastNextData').addEventListener('click', function (e) {
        for (var i = 0; i < 5; i++) {
            getNextData();
        }
    });

    document.getElementById('downloadCSVRaw').addEventListener('click', function (e) {
        e.preventDefault();
        var element = document.createElement('a');
        var text= "";
        measurements.forEach(data => {
            text += (data["dateTime"] + "," + data["systolic"] + "," + data["diastolic"] + "," + data["heartRate"] + "\n");
        });
        element.setAttribute('href', 'data:text/csv;charset=utf-8,' + encodeURIComponent(text));
        element.setAttribute('download', measurements[0]["User_idUser"] + "_RAW.csv");
        element.style.display = 'none';
        document.body.appendChild(element);
        element.click();
        document.body.removeChild(element);
    });

    document.getElementById('downloadCSVStat').addEventListener('click', function (e) {
        e.preventDefault();
        params = (new URL(document.location)).searchParams;
        idUser = params.get("idUser");
        oReq = new XMLHttpRequest();
        sendParams = new URLSearchParams();
        if(idUser !== null) {
            sendParams.append('idUser', idUser);
        }
        oReq.open("GET", "/api/pressureStat.php?" + sendParams.toString());
        oReq.send();
        oReq.onreadystatechange = function() {
            if(this.readyState != 4) {
                return;
            }
            // console.log("status: " + this.status);
            // console.log("response: " + this.response);
            if (this.status == 200) {
                stats = JSON.parse(this.response);
                if(stats) {
                    var element = document.createElement('a');
                    var text= "";
                    stats.forEach(data => {
                        text += (data["avg_sys_all"] + "," + data["avg_dia_all"]  + "," + data["dev_sys_all"] + "," + data["dev_dia_all"] + "," +
                            data["avg_sys_1_month"] + "," + data["avg_dia_1_month"]  + "," + data["dev_sys_1_month"] + "," + data["dev_dia_1_month"] + "," +
                            data["avg_sys_1_week"] + "," + data["avg_dia_1_week"]  + "," + data["dev_sys_1_week"] + "," + data["dev_dia_1_week"] + "," +
                            data["count_all"] + "," + data["count_1_month"]  + "," + data["count_1_week"] +
                            "\n");
                    });
                    element.setAttribute('href', 'data:text/csv;charset=utf-8,' + encodeURIComponent(text));
                    element.setAttribute('download', measurements[0]["User_idUser"] + "_STAT.csv");
                    element.style.display = 'none';
                    document.body.appendChild(element);
                    element.click();
                    document.body.removeChild(element);
                }
            } else if(this.status == 400){
                console.log("Error 400: something went wrong");
            } else if(this.status == 500){
                console.log("Error 500: something went wrong");
            }
        }
    });

});