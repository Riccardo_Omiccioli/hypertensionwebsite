function copy(text) {
    navigator.clipboard.writeText(text);
    console.log(text);
}

$(document).ready(function(){

    document.getElementById('idUser').addEventListener('input', function (e) {
        var input = $(this).val().split("-").join(""); // remove hyphens
        if (input.length > 0) {
        input = input.match(new RegExp('.{1,3}', 'g')).join("-");
        }
        // Refresh input
        $(this).val(input);
    });

    document.getElementById('searchSubmit').addEventListener('click', function (e) {
        e.preventDefault();
        var searchString = document.getElementById('idUser').value;
        // TODO: Add check on length and regex
        if (searchString !== null) {
            var sendParams = new URLSearchParams();
            sendParams.append('idUser', searchString);
            window.location.href = "/data.php?" + sendParams.toString();
        }
    });


    var table = document.getElementById("usersTable");
    var rows = table.getElementsByTagName("tr");
    for (i = 1; i < rows.length; i++) {
        var currentRow = table.rows[i];
        let id = currentRow.getElementsByTagName("td")[0].innerText;
        currentRow.addEventListener('click', function (e) {
            var sendParams = new URLSearchParams();
            sendParams.append('idUser', id);
            window.location.href = "/data.php?" + sendParams.toString();
        });

        let copyButton = currentRow.getElementsByTagName("td")[0].getElementsByTagName("button")[0];
        copyButton.addEventListener('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            navigator.clipboard.writeText(id);
            // var svgObject = button.getElementsByTagName("object")[0].contentDocument;
            // console.log(svgObject);
            // svg = svgObject.getElementById("copy").style.fill = "red";
            // console.log(svg);
        });

        let input = currentRow.getElementsByTagName("td")[0].getElementsByTagName("form")[0].getElementsByTagName("input")[0];
        input.addEventListener('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            console.log("Clicked " + id + " input");
        });

        let saveButton = currentRow.getElementsByTagName("td")[0].getElementsByTagName("form")[0].getElementsByTagName("button")[0];
        saveButton.style.display = "none";
        input.addEventListener('input', function (e) {
            saveButton.style.display = "inline-block";
        });
        saveButton.addEventListener('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            console.log("Save: " + id + " label: " + input.value);
            var sendParams = new URLSearchParams();
            sendParams.append('idUser', id);
            sendParams.append('label', input.value);
            var oReq = new XMLHttpRequest();
            oReq.open("POST", "/api/saveUserLabel.php");
            oReq.send(sendParams);
            oReq.onreadystatechange = function() {
                if(this.readyState != 4) {
                    return;
                }
                console.log(this.status);
                console.log(this.response);
                if (this.status == 200) {
                    //window.location.href = "/";
                    // location.reload();
                    // window.location.reload();
                    console.log("Saved");
                    saveButton.style.display = "none";
                } else if(this.status == 400){
                    console.log("Error 400: something went wrong");
                } else if(this.status == 500){
                    console.log("Error 500: something went wrong");
                }
            }
        });
        // svg = button.getElementsByTagName("object")[0].contentDocument.getElementById("copy");
        // button.addEventListener('mouseover', function (e) {
        //     console.log(e);
        //     button.getElementsByTagName("object")[0].contentDocument.getElementById("copy").style.fill = "white";
        // });
        // button.addEventListener('mouseout', function (e) {
        //     console.log(e);
        //     button.getElementsByTagName("object")[0].contentDocument.getElementById("copy").style.fill = "black";
        // });
        //   var svgObject = row.getElementsByTagName("td")[0].getElementsByTagName("object")[0].style.fill = "white";
        //svgObject.setAttribute("fill", "rgb(200,200,0)");
        //svgObject.setAttribute("fill", "rgb(255,255,0)");
        // currentRow.addEventListener('mouseover', function (e) {
        //     svg.style.fill = "white";
        // });
        // currentRow.addEventListener('mouseout', function (e) {
        //     currentRow.getElementsByTagName("td")[0].getElementsByTagName("button")[0].getElementsByTagName("object")[0].contentDocument.getElementById("copy").style.fill = "black";
        // });
    }

});