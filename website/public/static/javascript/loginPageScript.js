$(document).ready(function(){

    $(document.getElementById("loginShowPassword")).click(function(event){
        event.preventDefault();
        if($(document.getElementById("loginPassword")).attr("type") == "password") {
            $(document.getElementById("loginPassword")).attr("type", "text");
        } else {
            $(document.getElementById("loginPassword")).attr("type", "password");
        }
    })

    $(document.getElementById("registerShowPassword")).click(function(event){
        event.preventDefault();
        if($(document.getElementById("registerPassword")).attr("type") == "password") {
            $(document.getElementById("registerPassword")).attr("type", "text");
            $(document.getElementById("registerRePassword")).attr("type", "text");
        } else {
            $(document.getElementById("registerPassword")).attr("type", "password");
            $(document.getElementById("registerRePassword")).attr("type", "password");
        }
    })

    $(document.getElementById("loginSubmit")).click(function(event){
        event.preventDefault();
        var errorMessage = document.getElementById("loginErrorMessage");
        var regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$/;
        var regexPassword = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*\_\-])(?=.{8,})/;
        var email = document.getElementById("loginEmail").value;
        var password = document.getElementById("loginPassword").value;
        if(email === "") {
            errorMessage.textContent = "Inserire email";
            errorMessage.classList.add("errorMessageShow");
        } else if(!email.match(regexEmail)) {
            errorMessage.textContent = "Inserire email valida";
            errorMessage.classList.add("errorMessageShow");
        } else if(password === "") {
            errorMessage.textContent = "Inserire password";
            errorMessage.classList.add("errorMessageShow");
        } else if(!password.match(regexPassword)) {
            errorMessage.textContent = "Inserire password valida";
            errorMessage.classList.add("errorMessageShow");
        } else if(email.match(regexEmail) && password.match(regexPassword)) {
            errorMessage.textContent = "";
            var sendParams = new URLSearchParams();
            sendParams.append('email', email);
            sendParams.append('password', password);

            var oReq = new XMLHttpRequest();
            oReq.open("POST", "/api/login.php");
            oReq.send(sendParams);

            oReq.onreadystatechange = function() {
                if(this.readyState != 4) {
                    return;
                }
                console.log(this.status);
                console.log(this.response);
                if (this.status == 200) {
                    window.location.href = "/";
                } else if(this.status == 400){
                    errorMessage.textContent = "Errore: richiesta non valida";
                    errorMessage.classList.add("errorMessageShow");
                } else if(this.status == 401){
                    errorMessage.textContent = "Errore: email o password errati";
                    errorMessage.classList.add("errorMessageShow");

                } else if(this.status == 500){
                    errorMessage.textContent = "Errore: qualcosa è andato storto";
                    errorMessage.classList.add("errorMessageShow");
                }
            }
        }
    });

    $(document.getElementById("registerSubmit")).click(function(event){
        event.preventDefault();
        var errorMessage = document.getElementById("registerErrorMessage");
        var regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$/;
        var regexPassword = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*\_\-])(?=.{8,})/;
        var email = document.getElementById("registerEmail").value;
        var password = document.getElementById("registerPassword").value;
        var rePassword = document.getElementById("registerRePassword").value;
        var consent = document.getElementById("consent").checked;
        if(email === "") {
            errorMessage.textContent = "Inserire email";
            errorMessage.classList.add("errorMessageShow");
        } else if(!email.match(regexEmail)) {
            errorMessage.textContent = "Inserire email valida";
            errorMessage.classList.add("errorMessageShow");
        } else if(password === "") {
            errorMessage.textContent = "Inserire password";
            errorMessage.classList.add("errorMessageShow");
        } else if(!password.match(regexPassword)) {
            errorMessage.textContent = "Inserire password valida: sono richiesti almeno 8 caratteri, 1 carattere minuscolo, 1 carattere maiuscolo, 1 carattere numerico e 1 carattere speciale";
            errorMessage.classList.add("errorMessageShow");
        } else if(password.match(regexPassword) && rePassword === "") {
            errorMessage.textContent = "Inserire conferma password";
            errorMessage.classList.add("errorMessageShow");
        } else if(password.match(regexPassword) && password !== rePassword) {
            errorMessage.textContent = "La password e la conferma della password non coincidono";
            errorMessage.classList.add("errorMessageShow");
        } else if(consent === false) {
            errorMessage.textContent = "Devi fornire il consenso per proseguire";
            errorMessage.classList.add("errorMessageShow");
        } else if(email.match(regexEmail) && password.match(regexPassword) && password === rePassword && consent === true) {
            var sendParams = new URLSearchParams();
            sendParams.append('email', email);
            sendParams.append('password', password);
            sendParams.append('consent', consent);

            var oReq = new XMLHttpRequest();
            oReq.open("POST", "/api/register.php");
            oReq.send(sendParams);

            oReq.onreadystatechange = function() {
                if(this.readyState != 4) {
                    return;
                }
                console.log(this.status);
                console.log(this.response);
                if (this.status == 200) {
                    window.location.href = "/";
                } else if(this.status == 400){
                    errorMessage.textContent = "Errore 400: qualcosa è andato storto";
                    errorMessage.classList.add("errorMessageShow");
                } else if(this.status == 401){
                    errorMessage.textContent = "Errore 401: compila tutti i campi correttamente";
                    errorMessage.classList.add("errorMessageShow");
                } else if(this.status == 500){
                    errorMessage.textContent = "Errore 500: qualcosa è andato storto";
                    errorMessage.classList.add("errorMessageShow");
                }
            }
        }
    });
});