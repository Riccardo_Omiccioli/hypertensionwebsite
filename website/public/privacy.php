<?php
	require_once($_SERVER['DOCUMENT_ROOT']."/../private/path.php");
    $params["page"] = PAGE."privacy.php";
    $params["title"] = "Privacy";
    $params["css"] = ["mainStyleSheet.css" , "privacySheet.css"];
	$params["scriptjs"] = [ "mainPageScript.js"];
    require_once(TEMPLATE."base.php");
?>