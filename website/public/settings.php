<?php

	if(!isset($_SESSION)) {
		session_start();
	}

	if (!isset($_SESSION["user"])){
		header("location: login.php");
		exit;
	}

	require_once($_SERVER['DOCUMENT_ROOT']."/../private/path.php");
    $params["page"] = PAGE."settings.php";
    $params["title"] = "Impostazioni";
    $params["css"] = ["mainStyleSheet.css", "settingsStyleSheet.css"];
	$params["scriptjs"] = [ "mainPageScript.js" , "settingsPageScript.js"];

	if(isset($_POST["username"]) && isset($_POST["password"])){
		//Check if user exists and is logged in
		echo "User?";
	}

    require_once(TEMPLATE."base.php");

?>