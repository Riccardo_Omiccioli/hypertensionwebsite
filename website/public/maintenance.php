<?php

    if(isset($_SESSION)) {
        session_start();
    }

	require_once($_SERVER['DOCUMENT_ROOT']."/../private/path.php");
    $params["page"] = PAGE."maintenance.php";
    $params["title"] = "";
    $params["css"] = ["mainStyleSheet.css"];
	$params["scriptjs"] = [ "mainPageScript.js"];
    require_once(TEMPLATE."base.php");

?>