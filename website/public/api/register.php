<?php

require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

use Database\User as User;

if (!isset($_POST["email"]) || !isset($_POST["password"]) || !isset($_POST["consent"])) {
	http_response_code(400);
	exit();
}

$result = null;
try {
    if($_POST["consent"] === 'true') {
        $user = new User(null, null, $_POST["email"], password_hash($_POST["password"], PASSWORD_DEFAULT), null, 0, 0);
        $result = $user->save();
    }
} catch (Exception $e) {
	http_response_code(500);
	exit();
}
if ($result !== null) {
    session_start();
    $_SESSION["user"] = $result;
    http_response_code(200);
    exit();
} else {
	http_response_code(401);
	exit();
}

http_response_code(500);
die();

?>