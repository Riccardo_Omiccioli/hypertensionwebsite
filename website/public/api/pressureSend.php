<?php

require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

use Database\Measurement as Measurement;
use \Database\User as User;

if (!isset($_SESSION)) {
    session_start();
}

// Check if systolic and diastolic values are set and not empty
if (!isset($_POST["systolic"]) || !isset($_POST["diastolic"]) || $_POST["systolic"] === "" || $_POST["diastolic"] === "") {
	http_response_code(400);
	exit();
}

// Check if user is set and exists in the database
if (!isset($_SESSION["user"]) || User::get($_SESSION["user"]) === null) {
	http_response_code(401);
	exit();
}

$result = null;
try {
    $measurement = new Measurement(null, $_POST["systolic"], $_POST["diastolic"], $_POST["heartRate"], (new DateTime("now"))->format("c"), $_SESSION["user"]);
    $result = $measurement->save();
    if ($result !== null) {
        http_response_code(200);
        exit();
    } else {
        http_response_code(401);
        exit();
    }
} catch (Exception $e) {
	http_response_code(500);
	exit();
}

http_response_code(500);
die();

?>