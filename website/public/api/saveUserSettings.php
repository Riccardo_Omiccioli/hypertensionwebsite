<?php

session_start();
require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

use \Database\User as User;

if(!isset($_POST["email"]) || !isset($_POST["password"])) {
	http_response_code(400);
	exit();
}

if (!isset($_SESSION["user"]) || User::get($_SESSION["user"]) === null) {
	http_response_code(401);
	exit();
}

$result = null;
try {
	$user = User::get($_SESSION["user"]);
	// Login with old email and password
	$id = User::login($user->getEmail(), $_POST["password"]);
	// Check id of session user and login user
    if($id !== null && $id === $user->getID()) {
		// Updating email
		$user->setEmail($_POST["email"]);
		// Updating password if a new password is present
		if(isset($_POST["newPassword"])) {
			$user->setPassword(password_hash($_POST["newPassword"], PASSWORD_DEFAULT));
		}
		// Linking accounts if a link code is present
		if(isset($_POST["linkCode"]) && User::getLinkUser($_POST["linkCode"])) {
			$user->link($_POST["linkCode"]);
			session_destroy();
			unset($_SESSION["user"]);
			http_response_code(204);
			exit();
		}
		$result = $user->save();
		http_response_code(200);
		exit();
	}
} catch (Exception $e) {
	http_response_code(500);
	exit();
}
if ($result !== null) {
    http_response_code(200);
    exit();
} else {
	http_response_code(400);
	exit();
}

http_response_code(500);
die();

?>
