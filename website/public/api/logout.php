<?php

require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

session_start();

if (!isset($_SESSION["user"])) {
	http_response_code(400);
	exit();
} else {
    session_destroy();
    unset($_SESSION["user"]);
    http_response_code(200);
    exit();
}

http_response_code(500);
die();

?>