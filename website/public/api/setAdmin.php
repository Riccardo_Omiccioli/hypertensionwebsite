<?php

session_start();
require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

use \Database\User as User;

if (!isset($_SESSION["user"]) || User::get($_SESSION["user"]) === null || !isset($_POST["idUser"]) || User::get($_POST["idUser"]) === null) {
	http_response_code(401);
	exit();
}

$result = null;
try {
	$user = User::get($_POST["idUser"]);
    if($user->getIdAdmin()) {
        // Remove user as admin
        $result = User::removeAdmin($user->getID());
        http_response_code(201);
        exit();
    } else {
        // Add user as admin
        $result = User::addAdmin($user->getID());
        http_response_code(200);
        exit();
    }
} catch (Exception $e) {
	http_response_code(500);
	exit();
}

http_response_code(500);
die();

?>
