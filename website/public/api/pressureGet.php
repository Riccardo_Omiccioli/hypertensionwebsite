<?php

require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

use Database\User as User;
use Database\Measurement as Measurement;

if (!isset($_SESSION)) {
    session_start();
}

if (!isset($_SESSION["user"]) || User::get($_SESSION["user"]) === null) {
	http_response_code(400);
	exit();
}

$result = null;
try {
    if(isset($_GET["idUser"]) && User::get($_SESSION["user"])->getIdDoctor() !== 0) {
        $result = Measurement::getAll($_GET["idUser"]);
    } else {
        $result = Measurement::getAll($_SESSION["user"]);
    }
} catch (Exception $e) {
	http_response_code(500);
	exit();
}
if ($result !== null) {
    http_response_code(200);
    $response = json_encode($result);
    echo $response;
    exit();
} else {
	http_response_code(401);
	exit();
}

http_response_code(500);
die();

?>