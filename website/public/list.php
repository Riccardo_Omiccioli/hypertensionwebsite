<?php

    use \Database\User as User;
    use \Util as Util;

    if(!isset($_SESSION)) {
        session_start();
    }

    if (!isset($_SESSION["user"])){
        header("location: login.php");
        exit;
    }

	require_once($_SERVER['DOCUMENT_ROOT']."/../private/path.php");
    $params["page"] = PAGE."list.php";
    $params["title"] = "Lista";
    $params["css"] = ["mainStyleSheet.css", "listStyleSheet.css"];
	$params["scriptjs"] = [ "mainPageScript.js" , "listPageScript.js"];
    require_once(TEMPLATE."base.php");

?>