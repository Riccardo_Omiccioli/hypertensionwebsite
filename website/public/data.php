<?php

    if(!isset($_SESSION)) {
        session_start();
    }

    if (!isset($_SESSION["user"])){
        header("location: login.php");
        exit;
    }

	require_once($_SERVER['DOCUMENT_ROOT']."/../private/path.php");
    $params["page"] = PAGE."data.php";
    $params["title"] = "Dati";
    $params["css"] = ["mainStyleSheet.css", "dataStyleSheet.css"];
	$params["scriptjs"] = [ "mainPageScript.js" , "dataPageScript.js"];
    require_once(TEMPLATE."base.php");

?>