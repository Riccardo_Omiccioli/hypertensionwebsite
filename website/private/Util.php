<?php

class Util {

    const UID_CHARS = "0123456789ABCDEFGHJKMNPQRSTUVWXYZ";

    public static function dump($var) {
        ob_start();
        var_dump($var);
        $res = ob_get_contents();
        ob_end_clean();
        return $res;
    }

    public static function uid() {
        $charactersLength = strlen(self::UID_CHARS);
        $randomString = '';
        for ($i = 0; $i < 12; $i++) {
            $randomString .= self::UID_CHARS[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function checkUid(String $var) {
        $uid = strtoupper($var);
        $pattern = ['/O/', '/[IL]/', '/[\- _]+/'];
        $replacement = ['0', '1', ''];
        $uid = preg_replace($pattern, $replacement, $uid);
        $result = preg_match("/[^".self::UID_CHARS."]/", $uid);
        if($result === 1 || strlen($uid) !== 12) {
            throw new \Exception("invalid idUser");
        }
        if ($result === false) {
            throw new \Exception("something went wrong");
        }
        return $uid;
    }

    public static function checkLinkCode(String $var) {
        $uid = strtoupper($var);
        $pattern = ['/O/', '/[IL]/', '/[\- _]+/'];
        $replacement = ['0', '1', ''];
        $uid = preg_replace($pattern, $replacement, $uid);
        $result = preg_match("/[^".self::UID_CHARS."]/", $uid);
        if($result === 1 || strlen($uid) !== 18) {
            throw new \Exception("invalid linkCode");
        }
        if ($result === false) {
            throw new \Exception("something went wrong");
        }
        return $uid;
    }

}

?>