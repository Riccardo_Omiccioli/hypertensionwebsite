<?php
namespace Database;

require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

class DatabaseReader {
	const DB_DBNAME   = "chatbotdb";
	const DB_IP       = "localhost";
	const DB_PORT     = 5432;

	protected $connection;

	private static ?DatabaseReader $dbr = null;

	protected function __construct(?String $username, ?String $password) {
		$this->connection = \pg_connect("host=".static::DB_IP." port=".static::DB_PORT." dbname=".static::DB_DBNAME." user=$username password=$password");
	}

	public function prepare(String $stmt) {
		return \pg_prepare($this->connection, "", $stmt);
	}

	public function query(String $query) {
		//return $this->connection->query($query);
		return \pg_query($this->connection, $query);
	}

	public function execute(Array $params) {
		return \pg_execute($this->connection, "", $params);
	}

	public static function get() {
		if (self::$dbr === null) {
			$credentials = \Credentials::getReaderCredentials();
			self::$dbr = new DatabaseReader(...$credentials);
		}
		return self::$dbr;
	}
}
?>
