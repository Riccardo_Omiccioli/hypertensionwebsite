<?php

namespace Database;
require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

use Database\DatabaseReader as DatabaseReader;
use Database\DatabaseWriter as DatabaseWriter;

use \Util as Util;

class Measurement {

	private ?int $idMeasurement;
	private int $systolic;
	private int $diastolic;
	private ?int $heartRate;
	private string $dateTime;
	private string $User_idUser;

	public function __construct(?int $idMeasurement, int $systolic, int $diastolic, ?int $heartRate, string $dateTime, string $User_idUser) {
		$this->setIDMeasurement($idMeasurement);
		$this->setSystolic($systolic);
		$this->setDiastolic($diastolic);
		$this->setHeartRate($heartRate);
		$this->setDateTime($dateTime);
		$this->setUser_idUser($User_idUser);
	}

	public function setIDMeasurement(?int $idMeasurement) {
		$this->idMeasurement = $idMeasurement;
	}

	public function getIDMeasurement(){
		return $this->idMeasurement;
	}

	public function setSystolic(int $systolic) {
		$this->systolic = $systolic;
	}

	public function getSystolic(){
		return $this->systolic;
	}

	public function setDiastolic(int $diastolic){
		$this->diastolic = $diastolic;
	}

	public function getDiastolic(){
		return $this->diastolic;
	}

	public function setHeartRate(?int $heartRate){
		$this->heartRate = $heartRate;
	}

	public function getHeartRate(){
		return $this->heartRate;
	}

	public function setDateTime(string $dateTime){
		$this->dateTime = $dateTime;
	}

	public function getDateTime(){
		return $this->dateTime;
	}

	public function setUser_idUser(string $User_idUser) {
		$this->User_idUser = $User_idUser;
	}

	public function getUser_idUser() {
		return $this->User_idUser;
	}

	public function save() {
		$db = DatabaseWriter::get();
		$db->transaction();
		$db->prepare('INSERT INTO users."Measurement" ("systolic", "diastolic", "heartRate", "dateTime", "User_idUser") VALUES ($1, $2, $3, $4, $5);');
		$db->execute([$this->systolic, $this->diastolic, $this->heartRate, $this->dateTime, $this->User_idUser]);
		$db->prepare('INSERT INTO users."UserStat" ("websiteInsertions", "User_idUser") VALUES (1, $1) '.
			'ON CONFLICT ("User_idUser") DO UPDATE SET "websiteInsertions" = users."UserStat"."websiteInsertions" + 1;');
		$result = $db->execute([$this->User_idUser]);
		$db->commit();
		return $result;
	}

	// TODO: Check return
	public static function get($id) {
		$id = Util::checkUid($id);
		$db = DatabaseWriter::get();
		$query = 'SELECT * FROM users."Measurement" '.
			'WHERE "Measurement"."User_idUser" = $1;';
		$db->prepare($query);
		$result = $db->execute([$id]);
		$row = pg_fetch_row($result);
		return $row ? new Measurement(...$row) : null;
	}

	public static function getMean($id) {
		$id = Util::checkUid($id);
		$db = DatabaseWriter::get();
		$query = 'SELECT *, (SELECT COUNT(*) FROM users."Measurement" WHERE "User_idUser" = $1 AND "dateTime" > (now() - interval \'6 month\')) AS "c6m", '.
		'(SELECT COUNT(*) FROM users."Measurement" WHERE "User_idUser" = $1 AND "dateTime" > (now() - interval \'3 month\')) AS "c3m", '.
		'(SELECT COUNT(*) FROM users."Measurement" WHERE "User_idUser" = $1 AND "dateTime" > (now() - interval \'1 month\')) AS "c1m", '.
		'(SELECT COUNT(*) FROM users."Measurement" WHERE "User_idUser" = $1 AND "dateTime" > (now() - interval \'1 week\')) AS "c1w" '.
		'FROM (SELECT CAST(AVG("systolic") AS int) AS "s6m", CAST(AVG("diastolic") AS int) AS "d6m" FROM users."Measurement" WHERE "User_idUser" = $1 AND "dateTime" > (now() - interval \'6 month\')) AS "avg6month" '.
		'JOIN (SELECT CAST(AVG("systolic") AS int) AS "s3m", CAST(AVG("diastolic") AS int) AS "d3m" FROM users."Measurement" WHERE "User_idUser" = $1 AND "dateTime" > (now() - interval \'3 month\')) AS "avg3month" ON 1=1 '.
		'JOIN (SELECT CAST(AVG("systolic") AS int) AS "s1m", CAST(AVG("diastolic") AS int) AS "d1m" FROM users."Measurement" WHERE "User_idUser" = $1 AND "dateTime" > (now() - interval \'1 month\')) AS "avg1month" ON 1=1 '.
		'JOIN (SELECT CAST(AVG("systolic") AS int) AS "s1w", CAST(AVG("diastolic") AS int) AS "d1w" FROM users."Measurement" WHERE "User_idUser" = $1 AND "dateTime" > (now() - interval \'1 week\')) AS "avg1week" ON 1=1;';
		$db->prepare($query);
		$result = $db->execute([$id]);
		return pg_fetch_all($result);
		// error_log("-----MEASUREMENTS: ".Util::dump($measurements)."\n");
	}

	public static function getStat($id) {
		$id = Util::checkUid($id);
		$db = DatabaseWriter::get();
		$query = 'SELECT *, '.
			'(SELECT COUNT(*) FROM users."Measurement" WHERE "User_idUser" = $1) AS "count_all", '.
			'(SELECT COUNT(*) FROM users."Measurement" WHERE "User_idUser" = $1 AND "dateTime" > (now() - interval \'1 month\')) AS "count_1_month", '.
			'(SELECT COUNT(*) FROM users."Measurement" WHERE "User_idUser" = $1 AND "dateTime" > (now() - interval \'1 week\')) AS "count_1_week" '.
			'FROM (SELECT CAST(AVG("systolic") AS int) AS "avg_sys_all", CAST(AVG("diastolic") AS int) AS "avg_dia_all" FROM users."Measurement" WHERE "User_idUser" = $1) AS "avgAll" '.
			'JOIN (SELECT ROUND(STDDEV("systolic"), 2) AS "dev_sys_all", ROUND(STDDEV("diastolic"), 2) AS "dev_dia_all" FROM users."Measurement" WHERE "User_idUser" = $1) AS "devAll" ON 1=1 '.
			'JOIN (SELECT CAST(AVG("systolic") AS int) AS "avg_sys_1_month", CAST(AVG("diastolic") AS int) AS "avg_dia_1_month" FROM users."Measurement" WHERE "User_idUser" = $1 AND "dateTime" > (now() - interval \'1 month\')) AS "avg1month" ON 1=1 '.
			'JOIN (SELECT ROUND(STDDEV("systolic"), 2) AS "dev_sys_1_month", ROUND(STDDEV("diastolic"), 2) AS "dev_dia_1_month" FROM users."Measurement" WHERE "User_idUser" = $1 AND "dateTime" > (now() - interval \'1 month\')) AS "dev1month" ON 1=1 '.
			'JOIN (SELECT CAST(AVG("systolic") AS int) AS "avg_sys_1_week", CAST(AVG("diastolic") AS int) AS "avg_dia_1_week" FROM users."Measurement" WHERE "User_idUser" = $1 AND "dateTime" > (now() - interval \'1 week\')) AS "avg1week" ON 1=1 '.
			'JOIN (SELECT ROUND(STDDEV("systolic"), 2) AS "dev_sys_1_week", ROUND(STDDEV("diastolic"), 2) AS "dev_dia_1_week" FROM users."Measurement" WHERE "User_idUser" = $1 AND "dateTime" > (now() - interval \'1 week\')) AS "dev1week" ON 1=1;';
		$db->prepare($query);
		$result = $db->execute([$id]);
		return pg_fetch_all($result);
		// error_log("-----MEASUREMENTS: ".Util::dump($measurements)."\n");
	}

	public static function getAll($id) {
		$id = Util::checkUid($id);
		$db = DatabaseWriter::get();
		$query = 'SELECT * FROM users."Measurement" '.
			'LEFT JOIN (select CAST(AVG("systolic") AS int) AS "avgSystolic", ROUND(STDDEV("systolic"), 2) AS "stdDevSystolic", CAST(AVG("diastolic") AS int) AS "avgDiastolic", ROUND(STDDEV("diastolic"), 2) AS "stdDevDiastolic", '.
				'date_part(\'year\', "dateTime") AS "y", date_part(\'week\', "dateTime") AS "w" FROM users."Measurement" '.
				'WHERE "Measurement"."User_idUser" = $1 GROUP BY "y", "w") '.
			'AS "b" ON date_part(\'year\', users."Measurement"."dateTime") = "y" AND date_part(\'week\', users."Measurement"."dateTime") = "w" '.
			'WHERE "Measurement"."User_idUser" = $1 '.
			'ORDER BY "Measurement"."dateTime" ASC;';
		$db->prepare($query);
		$result = $db->execute([$id]);
		return pg_fetch_all($result);
		// error_log("-----MEASUREMENTS: ".Util::dump($measurements)."\n");
	}
}

?>
