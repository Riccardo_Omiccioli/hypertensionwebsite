<?php

namespace Database;
require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

class DatabaseWriter extends DatabaseReader {

	private static ?DatabaseWriter $dbw = null;

	private bool $transacting = false;

	public function superTransaction() {
		$this->transacting = true;
		$this->transaction();
	}
	public function transaction() {
		\pg_query($this->connection, "BEGIN");
	}

	public function superCommit() {
		$this->transacting = false;
		$this->commit();
	}
	public function commit() {
		if (!$this->transacting) {
			\pg_query($this->connection, "COMMIT");
		}
	}

	public function rollback() {
		\pg_query($this->connection, "ROLLBACK");
		$this->transacting = false;
	}

	public static function get() {
		if (self::$dbw === null) {
			$credentials = \Credentials::getWriterCredentials();
			self::$dbw = new DatabaseWriter(...$credentials);
		}
		return self::$dbw;
	}
}
?>
