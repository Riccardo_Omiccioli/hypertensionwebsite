<?php

namespace Database;
require_once(dirname($_SERVER['DOCUMENT_ROOT']) . "/private/path.php");

use Database\DatabaseReader as DatabaseReader;
use Database\DatabaseWriter as DatabaseWriter;

use \Util as Util;

class User {

	private string $idUser;
	private ?string $idTelegram = null;
	private ?string $email = null;
	private ?string $password = null;
	private ?string $linkCode = null;
	private int $idAdmin = 0;
	private int $idDoctor = 0;

	public function __construct(?string $id, ?string $idTelegram, ?string $email, ?string $password, ?string $linkCode, ?int $idAdmin, ?int $idDoctor) {
		$this->setID($id ?? Util::uid());
		$this->setIDTelegram($idTelegram);
		$this->setEmail($email);
		$this->setPassword($password);
		$this->setLinkCode($linkCode);
		$this->setIdAdmin($idAdmin ?? 0);
		$this->setIdDoctor($idDoctor ?? 0);
	}

	public function setID(string $id) {
		$this->idUser = Util::checkUid($id);
	}

	public function getID(){
		return $this->idUser;
	}

	public function setIDTelegram(?string $idTelegram) {
		$this->idTelegram = $idTelegram;
	}

	public function getIDTelegram() {
		return $this->idTelegram;
	}

	public function setEmail(?string $email) {
		if ($email !== null && strlen($email) > 255) {
			throw new \LengthException("Email troppo lunga");
		}
		$this->email = $email;
	}

	public function getEmail() {
		return $this->email;
	}

	public function setPassword(?string $password) {
		if ($password !== null && strlen($password) > 255) {
			throw new \LengthException("Password troppo lunga");
		}
		$this->password = $password;
	}

	public function getPassword() {
		return $this->password;
	}

	public function setLinkCode(?string $linkCode) {
		if ($linkCode !== null && strlen($linkCode) > 255) {
			throw new \LengthException("Link code troppo lunga");
		}
		$this->linkCode = $linkCode;
	}

	public function getLinkCode() {
		return $this->linkCode;
	}

	public function setIdAdmin(int $id) {
		$this->idAdmin = $id;
	}

	public function getIdAdmin() {
		return $this->idAdmin;
	}

	public function setIdDoctor(int $id) {
		$this->idDoctor = $id;
	}

	public function getIdDoctor() {
		return $this->idDoctor;
	}

	public function save() {
		$db = DatabaseWriter::get();
		$db->transaction();
		$db->prepare('INSERT INTO users."User" VALUES ($1, $2, $3, $4, $5) '.
			'ON CONFLICT ("idUser") DO UPDATE SET "idTelegram" = excluded."idTelegram", "email" = excluded."email", "password" = excluded."password", "linkCode" = excluded."linkCode";');
		$db->execute([$this->idUser, $this->idTelegram, $this->email, $this->password, $this->linkCode]);
		if($this->idAdmin === 0) {
			$db->prepare('DELETE FROM users."Admin" WHERE "User_idUser" = $1;');
			$db->execute([$this->idUser]);
		} else {
			$db->prepare('INSERT INTO users."Admin" VALUES($1, $2) '.
				'ON CONFLICT ("User_idUser") DO UPDATE SET "idAdmin" = EXCLUDED."idAdmin";');
			$db->execute([$this->idAdmin, $this->idUser]);
		}
		if($this->idDoctor === 0) {
			$db->prepare('DELETE FROM users."Doctor" WHERE "User_idUser" = $1;');
			$db->execute([$this->idUser]);
		} else {
			$db->prepare('INSERT INTO users."Doctor" VALUES($1, $2) '.
				'ON CONFLICT ("User_idUser") DO UPDATE SET "idDoctor" = EXCLUDED."idDoctor";');
			$db->execute([$this->idDoctor, $this->idUser]);
		}
		$db->prepare('SELECT * FROM users."Consent" WHERE "User_idUser" = $1;');
		$result = $db->execute([$this->idUser]);
		$row = pg_fetch_row($result);
		if(!$row) {
			$db->prepare('INSERT INTO users."Consent" ("message", "date", "User_idUser") VALUES($1, $2, $3);');
			$db->execute(["Ho letto l'informativa privacy e acconsento alla memorizzazione dei dati secondo quanto stabilito", date("Y-m-d h:i:s"), $this->idUser]);
		}
		$db->commit();
		return $this->idUser;
	}

	public function link($linkCode) {
		$linkCode = Util::checkLinkCode($linkCode);
		$db = DatabaseWriter::get();
		$db->transaction();
		$db->prepare('UPDATE users."User" SET "email" = $1, "password" = $2 WHERE "linkCode" = $3;');
		$db->execute([$this->email, $this->password, $linkCode]);
		$db->prepare('UPDATE users."Measurement" SET "User_idUser" = (SELECT "idUser" FROM users."User" WHERE "linkCode" = $1) WHERE "User_idUser" = $2;');
		$db->execute([$linkCode, $this->idUser]);
		$db->prepare('DELETE FROM users."Consent" WHERE "User_idUser" = $1;');
		$db->execute([$this->idUser]);
		$db->prepare('UPDATE users."Doctor" SET "User_idUser" = (SELECT "idUser" FROM users."User" WHERE "linkCode" = $1) WHERE "User_idUser" = $2;');
		$db->execute([$linkCode, $this->idUser]);
		$db->prepare('UPDATE users."Admin" SET "User_idUser" = (SELECT "idUser" FROM users."User" WHERE "linkCode" = $1) WHERE "User_idUser" = $2;');
		$db->execute([$linkCode, $this->idUser]);
		$db->prepare('DELETE FROM users."UserLabel" WHERE "User_idUser_User" = $1;');
		$db->execute([$this->idUser]);
		$db->prepare('UPDATE users."UserStat" SET "telegramInsertions" = "telegramInsertions" + (SELECT "telegramInsertions" FROM users."UserStat" WHERE "User_idUser" = $2), '.
			'"websiteInsertions" = "websiteInsertions" + (SELECT "websiteInsertions" FROM users."UserStat" WHERE "User_idUser" = $2) WHERE "User_idUser" = (SELECT "idUser" FROM users."User" WHERE "linkCode" = $1);');
		$db->execute([$linkCode, $this->idUser]);
		$db->prepare('DELETE FROM users."UserStat" WHERE "User_idUser" = $1;');
		$db->execute([$this->idUser]);
		$db->prepare('DELETE FROM users."User" WHERE "idUser" = $1;');
		$result = $db->execute([$this->idUser]);
		$db->commit();
		return $result;
	}

	public static function removeUser($id) {
		$id = Util::checkUid($id);
		$db = DatabaseWriter::get();
		$db->transaction();
		$db->prepare('DELETE FROM users."Admin" WHERE "User_idUser" = $1');
		$db->execute([$id]);
		$db->prepare('DELETE FROM users."Doctor" WHERE "User_idUser" = $1');
		$db->execute([$id]);
		$db->prepare('DELETE FROM users."Measurement" WHERE "User_idUser" = $1');
		$db->execute([$id]);
		$db->prepare('DELETE FROM users."Alert" WHERE "User_idUser" = $1');
		$db->execute([$id]);
		$db->prepare('DELETE FROM users."Consent" WHERE "User_idUser" = $1');
		$db->execute([$id]);
		$db->prepare('DELETE FROM users."User" WHERE "idUser" = $1');
		$result = $db->execute([$id]);
		$db->commit();
		return $result;
	}

	/* Removes doctor permissions
	 * Params $id the idUser of the user to remove as doctor
	 */
	public static function removeDoctor($id) {
		$id = Util::checkUid($id);
		$db = DatabaseWriter::get();
		$db->transaction();
		$query = 'DELETE FROM users."Doctor" WHERE "User_idUser" = $1';
		$db->prepare($query);
		$result = $db->execute([$id]);
		$db->commit();
		return $result;
	}

	/* Sets doctor permissions
	 * Params $id the idUser of the user to set as doctor
	 */
	public static function addDoctor($id) {
		$id = Util::checkUid($id);
		$db = DatabaseWriter::get();
		$db->transaction();
		$query = 'INSERT INTO users."Doctor" ("User_idUser") VALUES ($1) '.
			'ON CONFLICT ("User_idUser") DO UPDATE SET "idDoctor" = EXCLUDED."idDoctor";';
		$db->prepare($query);
		$result = $db->execute([$id]);
		$db->commit();
		return $result;
	}

	/* Removes admin permissions
	 * Params $id the idUser of the user to remove as admin
	 */
	public static function removeAdmin($id) {
		$id = Util::checkUid($id);
		$db = DatabaseWriter::get();
		$db->transaction();
		$query = 'DELETE FROM users."Admin" WHERE "User_idUser" = $1';
		$db->prepare($query);
		$result = $db->execute([$id]);
		$db->commit();
		return $result;
	}

	/* Sets admin permissions
	 * Params $id the idUser of the user to set as admin
	 */
	public static function addAdmin($id) {
		$id = Util::checkUid($id);
		$db = DatabaseWriter::get();
		$db->transaction();
		$query = 'INSERT INTO users."Admin" ("User_idUser") VALUES ($1) '.
			'ON CONFLICT ("User_idUser") DO UPDATE SET "idAdmin" = EXCLUDED."idAdmin";';
		$db->prepare($query);
		$result = $db->execute([$id]);
		$db->commit();
		return $result;
	}

	public static function get($id) {
		$id = Util::checkUid($id);
		$db = DatabaseWriter::get();
		$query = 'SELECT "u".*, "a"."idAdmin", "d"."idDoctor" '.
			'FROM users."User" AS "u" LEFT JOIN users."Admin" AS "a" on "u"."idUser" = "a"."User_idUser" LEFT JOIN users."Doctor" AS "d" on "u"."idUser" = "d"."User_idUser" '.
			'WHERE "u"."idUser" = $1;';
		$db->prepare($query);
		$result = $db->execute([$id]);
		$row = pg_fetch_row($result);
		return $row ? new User(...$row) : null;
	}

	/* Gets user info as assoc array (to be used in admin panel)
	 * Params $id the idUser of the user to get informations about
	 */
	public static function getUserInfo($id) {
		$id = Util::checkUid($id);
		$db = DatabaseWriter::get();
		$query = 'SELECT "u"."idUser", "u"."idTelegram", "u"."email", "a"."idAdmin", "d"."idDoctor" '.
			'FROM users."User" AS "u" LEFT JOIN users."Admin" AS "a" on "u"."idUser" = "a"."User_idUser" LEFT JOIN users."Doctor" AS "d" on "u"."idUser" = "d"."User_idUser" '.
			'WHERE "u"."idUser" = $1;';
		$db->prepare($query);
		$result = $db->execute([$id]);
		return pg_fetch_assoc($result);
	}

	// Returns list of all users with their labels and stats according to current user
	public function getAll() {
		$db = DatabaseWriter::get();
		$query = 'SELECT "idUser", "idTelegram", "email", "label", "telegramInsertions", "websiteInsertions" FROM users."User" '.
			'LEFT JOIN (SELECT * FROM users."UserLabel" WHERE "UserLabel"."User_idUser_Doctor" = $1) as "l" ON "User"."idUser" = "l"."User_idUser_User" '.
			'LEFT JOIN users."UserStat" ON "User"."idUser" = "UserStat"."User_idUser" '.
			'WHERE "label" IS NULL OR "l"."User_idUser_Doctor" = $1 ORDER BY "label" ASC';
		$db->prepare($query);
		$result = $db->execute([$this->idUser]);
		return pg_fetch_all($result);
	}

	/* Saves a label for a couple of doctor-patient
	 * Params $idUser the ID of the patient
	 * Params $idDoctor the ID of the doctor
	 * Params $label the label to be saved
	 */
	public static function saveUserLabel($idUser, $idDoctor, $label) {
		$idUser = Util::checkUid($idUser);
		$idDoctor = Util::checkUid($idDoctor);
		$db = DatabaseWriter::get();
		$db->transaction();
		$query = 'INSERT INTO users."UserLabel" ("User_idUser_User", "User_idUser_Doctor", "label") VALUES ($1, $2, $3)'.
			'ON CONFLICT ("User_idUser_User", "User_idUser_Doctor") DO UPDATE SET "label" = $3;';
		$db->prepare($query);
		$result = $db->execute([$idUser, $idDoctor, $label]);
		$db->commit();
		return $result;
	}

	public static function getLinkUser($linkCode) {
		$linkCode = Util::checkLinkCode($linkCode);
		$db = DatabaseReader::get();
		$db->prepare('SELECT "idUser" FROM users."User" WHERE "linkCode" = $1;');
		$result = $db->execute([$linkCode]);
		$row = pg_fetch_assoc($result);
		if($row) {
			return $row["idUser"];
		}
		return null;
	}

	public static function login(String $email, String $password) {
		$db = DatabaseReader::get();
		$db->prepare('SELECT "idUser", "password" FROM users."User" WHERE "email" = $1;');
		$result = $db->execute([$email]);
		$row = pg_fetch_assoc($result);
		if($row && password_verify($password, $row["password"])) {
			return $row["idUser"];
		}
		return null;
	}

}

?>
