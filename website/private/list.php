<?php

use \Database\User as User;
use \Util as Util;

    if (!isset($_SESSION)) {
		session_start();
	}

    if (isset($_SESSION["user"])){
		$user = User::get($_SESSION["user"]);
        // If user is not a doctor redirect to index.php
        if ($user->getIdDoctor() == 0) {
            header("location: index.php");
            exit;
        }
    }

?>

<div>
    <form id="searchForm">
        <div>
            <label for="idUser">ID paziente</label>
            <input type="text" id="idUser" name="idUser" placeholder="ID paziente" required maxlength=15  value=""/>
            <button id="searchSubmit">
                <p>Cerca</p>
                <object data="<?php echo ICON;?>search.svg" type="image/svg+xml">search</object>
            </button>
        </div>
    </form>
    <div>
        <button id="previousData">
            <object data="<?php echo ICON;?>arrow_left.svg" type="image/svg+xml">previous</object>
        </button>
        <button id="nextData">
            <object data="<?php echo ICON;?>arrow_right.svg" type="image/svg+xml">next</object>
        </button>
    </div>
    <table id="usersTable">
        <thead>
            <tr>
                <th scope="col">ID paziente</th>
                <th scope="col">Telegram</th>
                <th scope="col">Sito</th>
            </tr>
        </thead>
        <tbody>
            <?php
                foreach($user->getAll() as $user) {
                    $label = $user["label"] ? $user["label"] : "";
                    $telegramInsertions = $user["telegramInsertions"] ? $user["telegramInsertions"] : 0;
                    $websiteInsertions = $user["websiteInsertions"] ? $user["websiteInsertions"] : 0;
                    $total = $telegramInsertions + $websiteInsertions;
                    if($total !== 0) {
                        $telegramInsertions = round($telegramInsertions / $total * 100, 1);
                        $websiteInsertions = round($websiteInsertions / $total * 100, 1);
                    }
                    $telegram = $user["idTelegram"] ? $telegramInsertions."%" : "";
                    $website = $user["email"] ? $websiteInsertions."%" : "";
                    echo '<tr><td>'.$user["idUser"].'<button><object id="copyObject" data="'.ICON.'copy.svg" type="image/svg+xml"></object></button>'.
                        '<form>'.
                            '<label for="userLabel">User label</label>'.
                            '<input type="text" id="userLabel" name="userLabel" value="'.$label.'" autocomplete="off">'.
                            '<button><object id="saveObject" data="'.ICON.'save.svg" type="image/svg+xml"></object></button>'.
                        '</form></td>'.
                        '<td>'.$telegram.'</td>'.
                        '<td>'.$website.'</td></tr>';
                }
            ?>
        </tbody>
    </table>
</div>
