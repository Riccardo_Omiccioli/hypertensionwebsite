<div>
    <h1>Effettua il login</h1>
    <form id="loginForm">
        <div>
            <label for="loginEmail">email</label>
            <input type="text" id="loginEmail" name="loginEmail" placeholder="Email" autocomplete="on"/>
        </div>
        <div>
            <label for="loginPassword">password</label>
            <input type="password" id="loginPassword" name="loginPassword" placeholder="Password" autocomplete="on"/>
        </div>
        <div>
            <button id="loginShowPassword">Mostra password</button>
            <input type="submit" id="loginSubmit" value="Accedi"/>
        </div>
    </form>
    <p id="loginErrorMessage"></p>
</div>
<div>
    <h1>Crea un nuovo account</h1>
    <form id="registerForm">
        <div>
            <label for="registerEmail">email</label>
            <input type="text" id="registerEmail" name="registerEmail" placeholder="Email" autocomplete="on"/>
        </div>
        <div>
            <label for="registerPassword">password</label>
            <input type="password" id="registerPassword" name="registerPassword" placeholder="Password"/>
        </div>
        <div>
            <label for="registerRePassword">conferma password</label>
            <input type="password" id="registerRePassword" name="registerRePassword"  placeholder="Conferma password"/>
        </div>
        <div>
            <label for="consent" id="consentLabel">Ho letto l'informativa privacy e acconsento alla memorizzazione dei dati secondo quanto stabilito</label>
            <input type="checkbox" id="consent" name="consent" required/>
        </div>
        <div>
            <button id="registerShowPassword">Mostra password</button>
            <input type="submit" id="registerSubmit" value="Crea Account"/>
        </div>
    </form>
    <p id="registerErrorMessage"></p>
</div>
