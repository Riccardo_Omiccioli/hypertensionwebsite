<?php

	use \Database\User as User;

	if(!isset($_SESSION)) {
		session_start();
	}

    if (isset($_SESSION["user"])){
		$user = User::get($_SESSION["user"]);
    }

	require_once($_SERVER['DOCUMENT_ROOT']."/../private/path.php");

?>

<!DOCTYPE html>
<html lang="it">
	<head>
		<meta charset="UTF-8"/>
		<meta name="description" content="Hypertension Chatbot Website"/>
		<meta name="keywords" content="Chatbot"/>
		<meta name="author" content="Riccardo Omiccioli"/>
		<meta http-equiv="ScreenOrientation" content="autoRotate:disabled">
		<base href="reference"/>
		<?php foreach ($params["css"] as $cssFile) : ?>
			<link type="text/css" rel="stylesheet" href="<?php echo CSS.$cssFile; ?>"/>
		<?php endforeach; ?>
		<title>
			Hypertension - <?php echo $params["title"];?>
		</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<?php foreach ($params["scriptjs"] as $jsFile) : ?>
			<!-- <script type="text/javascript">
				var phpString = "<?php //print($jsFile); ?>";
				console.log(phpString);
			</script> -->
			<script src="<?php echo JAVASCRIPT.$jsFile; ?>"></script>
		<?php endforeach; ?>
	</head>
	<body>
		<header>
			<?php
				if(isset($_SESSION["user"]) && $user !== null) {
					echo '<div id="menu">'.
						'<object id="menuObject" data="'.ICON.'menu.svg" type="image/svg+xml">menu</object>'.
						'</div>';
				}
			?>
			<div>
            	<a id="logo" href="/">
					<object id="logoObject" data="<?php echo ICON;?>logo.svg" type="image/svg+xml">logo</object>
				</a>
				<?php
					if(isset($_SESSION["user"]) && $user !== null && $params["page"] === PAGE."data.php") {
						echo '<div id="add">'.
							'<object id="addObject" data="'.ICON.'add.svg" type="image/svg+xml">add</object>'.
							'</div>';
					}
				?>
			</div>
		</header>
		<main>
			<aside>
				<ul>
					<li>
						<a href="/">Home</a>
					</li>
					<?php
						if(isset($user) && $user->getIdDoctor()) {
            				echo '<li>'.
									'<a href="/list.php">Lista pazienti</a>'.
								'</li>';
						}
					?>
					<?php
						if(isset($user) && $user->getIdAdmin()) {
            				echo '<li>'.
									'<a href="/admin.php">Gestione utenti</a>'.
								'</li>';
						}
					?>
					<li>
						<a href="/settings.php">Impostazioni account</a>
					</li>
					<li>
						<a id="logout">Logout</a>
					</li>
				</ul>
			</aside>
			<div>
				<?php require($params["page"]);?>
			</div>
		</main>
		<footer>
			<div>
				<p>2021 Hypertension Website by Riccardo Omiccioli</p>
			</div>
			<div>
				<p>info@hypertension.website</p>
				<a href="privacy.php">Privacy policy</a>
			</div>

		</footer>
	</body>
</html>