<?php

    use \Database\User as User;

    if (!isset($_SESSION)) {
		session_start();
	}

    if (isset($_SESSION["user"])){
		$user = User::get($_SESSION["user"]);
    }

?>

<div>
    <h1>Pannello gestione utenti</h1>
    <form id="settingsForm">
        <div>
            <label for="idUser">ID utente</label>
            <input type="text" id="idUser" name="idUser" placeholder="ID utente" maxlength=15/>
        </div>
        <div>
        <table id="userInfoTable">
            <thead>
            </thead>
            <tbody id="userInfoTableBody">
                <tr>
                    <th scope="row">ID Telegram</th>
                    <td id="idTelegramData"></td>
                </tr>
                <tr>
                    <th scope="row">Email</th>
                    <td id="emailData"></td>
                </tr>
                <tr>
                    <th scope="row">Dottore</th>
                    <td id="doctorData"></td>
                </tr>
                <tr>
                    <th scope="row">Admin</th>
                    <td id="adminData"></td>
                </tr>
            </tbody>
        </table>
    </div>
        <div>
            <button id="remove">Rimuovi</button>
            <button id="setDoctor">Imposta medico</button>
            <button id="setAdmin">Imposta admin</button>
        </div>
    </form>
    <p id="errorMessage" class="errorMessage"></p>
    <p id="successMessage" class="successMessage"></p>
</div>