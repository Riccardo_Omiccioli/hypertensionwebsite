<div>
	<section>
		<h2>Privacy Policy</h2>
		<p><strong>Informativa ai sensi dell'art. 13 del Codice della Privacy</strong></p>
		<p>
			I dati quali email e password personali sono utilizzati unicamente per il funzionamento del sistema e non sono soggetti in alcun modo a cessione ad enti terzi per alcun fine.
			I dati sanitari raccolti tramite il sistema sono utili alla sola cura del soggetto utente e vengono visualizzati solo dal personale medico tenuto al segreto professionale in accordo con l'articolo 9 del Gdpr.
		</p>
    </section>
    <section>
			<h4>Vi segnaliamo che il titolare del trattamento ad ogni effetto di legge è:</h4>
			<p><b><span id="azienda">Azienda:</span></b> azienda</p>
			<p>
				<b><span id="indirizzo">Indirizzo:</span></b> indirizzo<span id="cap"> - 00000</span> - <span id="citta">città</span> (<span id="provincia">XX</span>)
			</p>
			<p><b>Tel/Fax:</b><span id="telefono"> 00000000000</span></p>
			<p><b>E-mail:</b><span id="email"> info@hypertension.website</span></p>
		</section>
</div>