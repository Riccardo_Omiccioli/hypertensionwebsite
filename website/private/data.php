<?php

use \Database\User as User;
use \Database\Measurement as Measurement;
use \Util as Util;

    if (!isset($_SESSION)) {
		session_start();
	}
    if (isset($_SESSION["user"])){
		$user = User::get($_SESSION["user"]);
        $allMeasurements = Measurement::getAll($_SESSION["user"]);
    }

?>

<script type="text/javascript">
    var idUser='<?php echo isset($_SESSION["user"]) ? $_SESSION["user"] : '0';?>';
    console.log("idUser: " + idUser);
</script>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<h1>
    PAGINA IN MANUTENZIONE
    <?php
        date_default_timezone_set('Europe/Rome');
        echo date('d/m/Y H:i', time());
    ?>
</h1>
<div id="addDiv" style="display: none;">
    <form id="addForm">
        <div>
            <label for="systolicIn">sistolica</label>
            <input type="number" id="systolicIn" name="systolic" placeholder="Sistolica" min="0" max="500"/>
            <label for="diastolicIn">diastolica</label>
            <input type="number" id="diastolicIn" name="diastolic" placeholder="Diastolica" min="0" max="500"/>
            <label for="heartRateIn">frequenza</label>
            <input type="number" id="heartRateIn" name="heartRate" placeholder="Frequenza" min="0" max="500"/>
        </div>
        <div>
            <input type="submit" id="pressureSubmit" value="Aggiungi"/>
        </div>
    </form>
</div>
<div>
    <?php
        if(isset($user) && $user->getIdDoctor()) {
            echo "
                <form id=\"searchForm\">
                    <div>
                        <label for=\"idUser\">ID paziente</label>
                        <input type=\"text\" id=\"idUser\" name=\"idUser\" placeholder=\"ID paziente\" required maxlength=15 value=\"\">
                        <button id=\"searchSubmit\">
                            <p>Cerca</p>
                            <object data=\"".ICON."search.svg\" type=\"image/svg+xml\">search</object>
                        </button>
                    </div>
                </form>
            ";
        }
    ?>
    <canvas id="dataChart"></canvas>
    <div>
        <button id="fastPreviousData">
            <object data="<?php echo ICON;?>fast_arrow_left.svg" type="image/svg+xml">fastPrevious</object>
        </button>
        <button id="previousData">
            <object data="<?php echo ICON;?>arrow_left.svg" type="image/svg+xml">previous</object>
        </button>
        <button id="nextData">
            <object data="<?php echo ICON;?>arrow_right.svg" type="image/svg+xml">next</object>
        </button>
        <button id="fastNextData">
            <object data="<?php echo ICON;?>fast_arrow_right.svg" type="image/svg+xml">fastNext</object>
        </button>
    </div>
    <div>
        <table id="meanTable">
            <caption>Media utente: <?php echo isset($_SESSION["user"]) ? $_SESSION["user"] : 'null';?></caption>
            <thead>
                <tr>
                    <th scope="col">Media ultima settimana</th>
                    <th scope="col">Media ultimo mese</th>
                    <th scope="col">Media ultimi 3 mesi</th>
                    <th scope="col">Media ultimi 6 mesi</th>
                </tr>
            </thead>
            <tbody id="meanTableBody">
            </tbody>
        </table>
    </div>
    <div>
        <table id="dataTable">
            <caption>Dati utente: <?php echo isset($_SESSION["user"]) ? $_SESSION["user"] : 'null';?></caption>
            <thead>
                <tr>
                    <th scope="col">Data (GMT)</th>
                    <th scope="col">Sistolica</th>
                    <th scope="col">Diastolica</th>
                    <th scope="col">Frequenza</th>
                </tr>
            </thead>
            <tbody id="dataTableBody">
            </tbody>
        </table>
    </div>
    <div>
        <button id="downloadCSVRaw">
            <p>Download CSV raw</p>
        </button>
        <button id="downloadCSVStat">
            <p>Download CSV statistiche</p>
        </button>
    </div>
</div>
