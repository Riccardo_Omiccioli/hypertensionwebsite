<?php

    use \Database\User as User;

    if (!isset($_SESSION)) {
		session_start();
	}

    if (isset($_SESSION["user"])){
		$user = User::get($_SESSION["user"]);
    }

?>

<div>
    <h1>Account: <?php echo implode("-", str_split($user->getID(), 3))?></h1>
    <form id="settingsForm">
        <div>
            <label for="emailIn">email</label>
            <input type="text" id="emailIn" name="email" placeholder="Email" value="<?php echo $user->getEmail()?>"/>
        </div>
        <?php
            if($user->getIDTelegram() === null) {
                echo '<div>'.
                        '<label for="linkCodeIn">Codice link</label>'.
                        '<input type="text" id="linkCodeIn" name="linkCode" placeholder="Codice link"/>'.
                    '</div>';
            }
        ?>
        <div>
            <label for="passwordIn">Inserisci password attuale</label>
            <input type="password" id="passwordIn" name="password" placeholder="Password attuale"/>
        </div>
        <div>
		    <label for="newPasswordIn">Inserisci nuova password</label>
            <input type="password" id="newPasswordIn" name="newPassword" placeholder="Nuova password">
        </div>
        <div>
		    <label for="newRePasswordIn">Conferma nuova password</label>
            <input type="password" id="newRePasswordIn" name="newRePassword" placeholder="Conferma nuova password">
        </div>
        <div>
            <button id="showPassword">Mostra password</button>
            <input type="submit" id="save" value="Salva"/>
        </div>
    </form>
    <p id="errorMessage"></p>
    <p id="successMessage"></p>
</div>